import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { ThemeProvider } from '@material-ui/styles';
import React from 'react';
import 'typeface-roboto';
import './App.css';
import themeStyles from './styles/theme.js';

// Import Components
import About from './components/About.js';
import Contact from './components/Contact.js';
import Experience from './components/Experience.js';
import GridItem from './components/GridItem.js';
import Menu from './components/Menu.js';
import Skills from './components/Skills.js';
import Projects from './components/Projects.js';

// Import data
import contactData from './data/contact.json';
import menuData from './data/menu.json';
import skillData from './data/skills.json';
import experienceData from './data/experience.json';
import projectData from './data/projects.json';

const theme = themeStyles;

function App() {
  return (
    <div className = 'App'>
      <ThemeProvider theme={theme}>
        <Container id = 'site'>
        <Menu data = {menuData.menuItems}/>
        <Grid container spacing={0}> 
          <GridItem bgColour = 'primary.darkPurple' img_source = 'geometric_background.png' id = 'fixed_item' height = '100vh'/>
          <GridItem bgColour = 'secondary.sand' height = '70vh' id = 'hidden_item' fontColour = 'primary.blue' fontVariant = 'h1' text = "hidden"/>
          <About id = 'about' bgColour = 'secondary.sand' fontColour = 'primary.darkPurple' fontVariant = 'h2' text = "about"/>
          
          <GridItem bgColour = 'primary.darkPurple' fontColour = 'secondary.main' id = 'hidden_item' height = '100%' fontVariant = 'h1' text = "hidden"/>
          <Skills id = 'skills' data = {skillData} bgColour = 'secondary.blue' fontColour = 'primary.blue' fontVariant = 'h2' text = "skills"/>
          
          <GridItem bgColour = 'primary.darkPurple' fontColour = 'secondary.main' id = 'hidden_item' height = '100%' fontVariant = 'h1' text = "hidden"/>
          <Projects id = 'projects' data = {projectData} bgColour = 'secondary.main' fontColour = 'primary.main' fontVariant = 'h2' text = "projects"/>
          
          <GridItem bgColour = 'primary.darkPurple' fontColour = 'secondary.main' id = 'hidden_item' height = '100%' fontVariant = 'h1' text = "hidden"/>
          <Experience id = 'experience' data = {experienceData} bgColour = 'secondary.sand' fontColour = 'primary.darkPurple' fontVariant = 'h2' text = "experience"/>
          
          <GridItem bgColour = 'primary.darkPurple' fontColour = 'secondary.main' height = '50%' fontVariant = 'h1' text = "hidden" id='last_hidden'/>
          <Contact id = 'contact' bgColour = 'secondary.green' fontColour = 'primary.green' fontVariant = 'h2' text = "contact" data = {contactData}/>

        </Grid>
        </Container>
      </ThemeProvider>
    </div>
  );
}

export default App;
