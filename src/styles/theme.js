import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      darkPurple: '#491F35',
      main: '#5F4B66',
      sand: '#E7DBDB',
      green: '#536353',
      blue: '#56667A',
      contrastText: '#12090E',
      background: {
        paper: '#C2C3C6',
      },
    },
    secondary: {
      darkPurple: '#523D48',
      main: '#C7C0C9',
      sand: 'F0F0F1',
      green: '#DAE1DA',
      blue: '#C4C9D0',
      contrastText: '#2D2127',
    },
    tertiary: {
      main: '#B4A1BA',
    },
    font: {
      main: '#6C6368',
    }
    
  },
  typography: {
    h1: {
      fontFamily: "Monospace",
      fontWeight: 200,
      fontSize: "6rem",
      lineHeight: 1,
      letterSpacing: "-0.01562em",
      verticalAlign: 'middle',
    },
    h2: {
      fontFamily: "Monospace",
      fontWeight: 400,
      fontSize: "3rem",
      lineHeight: 1.167,
      letterSpacing: "-0.01562em",
      textAlign: "center"
    },
    subtitle1: {
      fontFamily: "Roboto",
      fontWeight: 400,
      fontSize: "1.3rem",
      lineHeight: 1.167,
      letterSpacing: "-0.01562em",
      textAlign: "left"
    },
    subtitle2: {
      fontFamily: "Roboto",
      fontWeight: 275,
      fontSize: "1rem",
      lineHeight: 1.3,
      letterSpacing: "-0.01562em",
      textAlign: "left",
      colorPrimary: '#6C6368'
    },
    subtitle3: {
      fontFamily: "Roboto",
      fontWeight: 200,
      fontSize: "0.9rem",
      lineHeight: 1.167,
      letterSpacing: "-0.01562em",
      textAlign: "left",
      colorPrimary: '#5F4B66'
    },
    body2: {
      fontFamily: "Roboto",
      fontWeight: 300,
      fontSize: "1rem",
      lineHeight: 1.2,
      letterSpacing: "-0.01562em",
      textAlign: "left",
      colorPrimary: '#6C6368'
    }
  }
});

export default theme;