import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  projectName: {
    fontSize: '15px',
    fontFamily: "Roboto",
    margin: '1%',
    borderBottom: '3px solid ' + theme.palette.tertiary.main,
    '&:hover': {
      background: theme.palette.tertiary.main
   }
  },
}))

export {useStyles}