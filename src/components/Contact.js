import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import '../App.css';

export default function Contact(props) {
    return (
      <Grid xs={6} id = {props.id} height = '50%'>
        <Box id = 'contact_box' bgcolor = {props.bgColour} textAlign="left" height = '50%' color={props.fontColour}>
          <Box p = '10%'>
            <Typography  p = '3%' variant={props.fontVariant}>{props.text}</Typography>
            <Divider variant="middle" />
            <Grid container justify = 'center' spacing = {8}> 
              <Grid item xs={12}></Grid>
                {props.data.items.map((item) => {
                  return(
                  <Grid item xs={3}>
                    <Typography align="center" variant = 'subtitle1' id = {item.id + '_icon'}>
                      <Link href = {item.hyperlink} target='blank' color="inherit" underline = 'none' id = {item.id + '_link'}>
                        {item.name}
                      </Link>
                    </Typography>
                  </Grid>
                  )
                })}

            </Grid>
          </Box>
        </Box>
      </Grid>
    )
  }


        