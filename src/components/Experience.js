import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import KeyboardArrowRightTwoToneIcon from '@material-ui/icons/KeyboardArrowRightTwoTone';
import React from 'react';
import '../App.css';

export default function Experience(props) {
    return (
    <Grid xs={6} id = {props.id} >
        <Box bgcolor = {props.bgColour} textAlign="left" height = '100%' color={props.fontColour}>
          <Box p = '10%'>
            <Typography  p = '3%' variant={props.fontVariant}>{props.text}</Typography>
            <Divider variant="middle" />
            <Grid container id='about_container' justify = 'space-around' spacing = {1}>
                <Grid item xs={11}>
                <List dense = 'true'>
                    <ListItem>
                        <ListItemText secondary = {props.data.subHeading}/>
                    </ListItem>
                </List>
                </Grid>  
            {props.data.experience.map((item) => {
                return(
                <Grid item xs={11}>
                    <Typography variant = 'subtitle1' color = 'primary'>
                        <p>{item.title}</p>
                    </Typography>
                    <Typography variant = 'subtitle2'>
                        {item.timeframe}
                    </Typography>
                    {item.description.map((descriptionItem) => {
                        return(
                            <List dense = 'true'>
                                <ListItem>
                                    <KeyboardArrowRightTwoToneIcon />
                                    <ListItemText secondary={descriptionItem}/>
                                </ListItem>
                            </List>
                        )
                    })}
                </Grid>
                )
            })}
            </Grid>
          </Box>
        </Box>
      </Grid>
    )
  }


        