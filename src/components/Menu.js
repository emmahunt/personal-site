import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import Box from '@material-ui/core/Box';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles } from '@material-ui/core/styles';
import CodeIcon from '@material-ui/icons/Code';
import HistoryIcon from '@material-ui/icons/History';
import InfoIcon from '@material-ui/icons/Info';
import PermContactCalendarIcon from '@material-ui/icons/PermContactCalendar';
import React, { useEffect } from 'react';



const useStyles = makeStyles(theme => ({
  root: {
    minWidth: '9%',
    backgroundColor: theme.palette.primary.darkPurple,
    position: 'fixed',
    left: '46%',
    top: '35%',
    zIndex: '500',
  },
  mobile:{
    backgroundColor: theme.palette.primary.darkPurple,
    zIndex: '500',
    position: 'fixed',
    left: '0',
    bottom: '0',
    width: '100%',
    color: theme.palette.primary.sand,
    '&$selected': {
      color: theme.palette.primary.main,
    },
  },
}));

export default function SelectedListItem(props) {
  const menuData = props.data;
  const icons = {"about": <InfoIcon/>, "skills" : <CodeIcon/>, "experience" : <HistoryIcon/>, "contact": <PermContactCalendarIcon/>}

  const classes = useStyles();
  const [selectedIndex, setSelectedIndex] = React.useState('');
  
  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
    var top = document.getElementById(index).offsetTop;
    window.scrollTo({
      top: top,
      behavior: 'smooth',
    })
  }
  
  const isInView = (element) => {
    const rect = element.getBoundingClientRect();
    return rect.bottom <= window.outerHeight;
  }
    
  const scrollSpy = e => {
    menuData
      .map(item => {
        var element = document.getElementById(item.id);
        if (element) {
          if (isInView(element)){
            setSelectedIndex(item.id)
          }
        }
      })
  }
          
  useEffect(() => {
    window.addEventListener("scroll", scrollSpy);
    return () => {
      window.removeEventListener("scroll", scrollSpy);
    };
  });   
  
  return (
    <Box color = '#E7DBDB'>
    <List  id='menu' component="nav" aria-label="menu" classes={{root:classes.root, active:classes.active}}>
      {
        menuData.map((menuItem) => (
          <ListItem button selected={selectedIndex===menuItem.id} onClick={event=> handleListItemClick(event, menuItem.id)} >
              <ListItemText primary={menuItem.name} align = 'center'/>
          </ListItem>
        ))
      }
    </List>
    
    <BottomNavigation value={selectedIndex} onChange={handleListItemClick} id = 'mobile_menu' className={classes.mobile}>
      {
        menuData.map((menuItem) => (
          <BottomNavigationAction onClick={event=> handleListItemClick(event, menuItem.id)}  icon={icons[menuItem.id]} value={menuItem.id} label={menuItem.name}/>
          ))
        }
    </BottomNavigation>
</Box>
  );
}
