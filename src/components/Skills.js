import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import FiberManualRecordOutlinedIcon from '@material-ui/icons/FiberManualRecordOutlined';
import React from 'react';
import '../App.css';

function skillIcon(name, score){
  var icons = []
  for(var i = 0; i < 4; i++){
    if(i<score){
      icons.push(<FiberManualRecordIcon/>);
    }
    else{
      icons.push(<FiberManualRecordOutlinedIcon/>)
    }
  }
  return(
      <ListItem>
        <ListItemText primary={name}/>
        {icons}
      </ListItem>
    );    
}

export default function About(props) {
    return (
      <Grid xs={6} id = {props.id} >
        <Box bgcolor = {props.bgColour} textAlign="left" height = '100%' color={props.fontColour}>
          <Box p = '10%'>
            <Typography  p = '3%' variant={props.fontVariant}>{props.text}</Typography>
            <Divider variant="middle" />
            <Grid container justify = 'space-around' spacing={1} alignItems = "flex-start">
                  {
                    Object.keys(props.data).map((category) => {
                      return(
                        <Grid item xs={5}>
                          <List dense = 'True'>
                            <Typography variant = 'subtitle1'>{category}</Typography>
                            {props.data[category].map((skill) => {
                              return(skillIcon(skill.name, skill.score))
                            })}
                          </List>
                        </Grid>
                      )
                    })
                  }
            </Grid>
          </Box>
        </Box>
      </Grid>
    )
  }

        