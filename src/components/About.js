import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import '../App.css';

export default function About(props) {
    return (
      <Grid xs={6} id = {props.id} >
        <Box bgcolor = {props.bgColour}  textAlign="left" height = '70%'>
          <Box p = '10%' color={props.fontColour}>
            <Typography  p = '3%' variant={props.fontVariant} >{props.text}</Typography>
            <Divider variant="middle" />
            <Grid container id='about_container' justify = 'center' spacing = {5}>
                <Grid item xs={6} justify = 'space-between' alignContent = 'left' id='about_text'>
                  <List>
                    <ListItem>
                      <ListItemText secondary = "Hello! I'm Emma - a Senior Analytics Engineer based in London.">
                      </ListItemText>
                    </ListItem>
                    <ListItem>
                      <ListItemText secondary = "I studied Maths and Computer Science at the University of Western Australia, and enjoy applying these skills to solve data and modelling problems. I love learning new skills and tools, and enjoy following all the cool new data tech that often arises in this industry! After working within Deloitte's Analytics Consulting team for 5 years, I joined the Data Design team at The Guardian for my next adventure with data.">
                      </ListItemText>
                    </ListItem>
                    <ListItem>
                      <ListItemText secondary = "In my spare time I like to knit, go running (anything for the excuse to post on Strava!) and keep up with the latests album releases in pop music.">
                      </ListItemText>
                    </ListItem>
                  </List>
                </Grid>
                <Grid item xs={5} justify = 'space-between'>
                <img src = 'emma_avatar.png' class = 'profile_img' alt = ''></img>
                </Grid>
            </Grid>
          </Box>
        </Box>
      </Grid>
    )
  }


      