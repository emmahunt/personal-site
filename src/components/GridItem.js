import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import '../App.css';

export default function GridItem(props) {
    return (
      <Grid xs={6} id = {props.id}>
        <Box bgcolor = {props.bgColour} textAlign="left" height = {props.height} color={props.fontColour}>
          <img src = {props.img_source} id = 'background_img'  alt = ''/>
          <Box p = '10%'>
            <Typography variant={props.fontVariant}>{props.text}</Typography>
          </Box>
        </Box>
      </Grid>
    )
  }
