import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import KeyboardArrowRightTwoToneIcon from '@material-ui/icons/KeyboardArrowRightTwoTone';
import List from '@material-ui/core/List';
import Link from '@material-ui/core/Link';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import '../App.css';

import {useStyles} from '../styles/Projects.styles.js';

export default function Contact(props) {
  const classes = useStyles();

  return (
    <Grid xs={6} id = {props.id} justify = "right">
      <Box id = 'contact_box' bgcolor = {props.bgColour} textAlign="left" color={props.fontColour}>
        <Box p = '10%'>
          <Typography  p = '3%' variant={props.fontVariant}>{props.text}</Typography>
          <Divider variant="middle" />
          <Grid container justify = 'center' spacing = {8}> 
            <Grid item xs={12}></Grid>
              {props.data.projects.map((project) => {
                return(
                <Grid container xs={10}>
                  <List dense = 'true'>
                    <Typography variant = 'subtitle1'>
                      {project.name}
                    </Typography>
                    <ListItem>
                      <Link className = {classes.projectName} underline = 'none' href = {project.hyperlink} target = '_blank'>{project.hyperlinkDescription}</Link>
                      |
                      <Link className = {classes.projectName} underline = 'none' href = {project.repo} target = '_blank'>code repo</Link>
                    </ListItem>

                    <ListItem>
                      <ListItemText secondary={project.description}/>
                    </ListItem>

                  </List>
                </Grid>
                )
              })}

          </Grid>
        </Box>
      </Box>
    </Grid>
  )
  }


        